#!/bin/bash

cd $(dirname $0)

echo CT-CHAT-DOCKER BUILD

docker ps -a | tr -s ' ' | grep ct-chat | cut -d' ' -f 1 | xargs -l1 docker kill 
docker ps -a | tr -s ' ' | grep ct-chat | cut -d' ' -f 1 | xargs -l1 docker rm 
ps -aux | grep "docker build" | grep ct | tr -s ' ' | cut -d' ' -f 2 | xargs -l1 kill 

yes | rm -r opt

cat ../package.json |  grep -v collabthings > collabthings-chat-package.json

mkdir -p opt/collabthings-chat
rsync -avz --exclude 'node_modules' --exclude '.git' --exclude 'testenv' --exclude "package-lock.json" --exclude "opt" --exclude "docker" .. opt/collabthings-chat

docker build . -t jeukku/ct-chat
