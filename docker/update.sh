#!/bin/bash

cd $(dirname $0)

rsync -avz --exclude 'node_modules' --exclude '.git' --exclude 'testenv' --exclude "package-lock.json" --exclude "opt" --exclude "docker" .. opt/collabthings-chat

for c in $(docker ps -a | grep chat | cut -d' ' -f1); do
	echo container ${c}
	docker cp opt/collabthings-chat/client ${c}:/opt/collabthings-chat/

	ls -l opt/collabthings-chat/client
	docker exec ${c} ls -l /opt/collabthings-chat/client
done
