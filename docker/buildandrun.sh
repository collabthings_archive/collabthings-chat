cd $(dirname $0)

bash rebuild.sh

docker network create -d bridge test-net

docker run  --name ct-chat1 -p 14882:14881 --network=test-net jeukku/ct-chat:latest &
docker run  --name ct-chat2 -p 14883:14881 --network=test-net jeukku/ct-chat:latest &

while ! curl http://localhost:14882/me; do
	echo "Waiting for http://localhost:14882/me"
	sleep 3;
done

while ! curl http://localhost:14883/me; do
	echo "Waiting for http://localhost:14883/me"
	sleep 1;
done

userid1=$(curl http://localhost:14882/me | jq -r '.userid')
userid2=$(curl http://localhost:14883/me | jq -r '.userid')

echo userid1 ${userid1} userid2 ${userid2} 

curl http://localhost:14882/users/follow/$(urlencode ${userid2})
curl http://localhost:14883/users/follow/$(urlencode ${userid1})

echo
echo "build and run done"




