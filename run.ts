import { CTApp, CTApi, CTSsb} from 'collabthings-api';
import CTChatApp from "./modules/app";

let ctapi: CTApi;
let ctapp: CTApp = new CTApp();
let chatapp: CTChatApp;

console.log( "Creating CTChatApp" );

ctapp.getSsb().addPlugin(require('./plugins/chat'));

ctapp.init().then(() => {
    console.log( "CTChatApp init" );

    ctapi = ctapp.getApi();

    chatapp = new CTChatApp(ctapp);

    console.log("CTChatApp register");
    chatapp.register(ctapi);
    
    console.log("CTChatApp start");
    ctapi.start();

    console.log( "CTChatApp created" );
    //ctapi.stop();
} ).catch(( err: string ) => {
    if ( err ) {
        console.log( "Run creation error " + err );
        process.exit( 1 );
    }
} );
