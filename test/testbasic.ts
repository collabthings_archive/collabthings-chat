/*
    Copyright (C) 2018  Juuso Vilmunen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

var assert = require( 'assert' );
import { CTApi, CTApp, CTSsb, CTAppInfo } from 'collabthings-api';
import CTChatClient from '../modules/client';

export default class BasicTests {
    aclient: CTChatClient;
    bclient: CTChatClient;

    constructor( aport: Number, bport: Number ) {
        this.aclient = new CTChatClient( aport );
        this.bclient = new CTChatClient( bport );
    }

    run() {
        //TODO tests should be using Api
        //        assert.equal("This is a chat! this.api.info().hello");
        this.aclient.sendMessage( "devtest", "testing1 " + new Date() );
        this.aclient.sendMessage( "devtest", "testing2 " + new Date() );
//        this.bclient.getMessages();
    }
}
