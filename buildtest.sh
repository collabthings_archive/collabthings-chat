#!/bin/bash

echo COLLABTHINGS-CHAT BUILD

bash docker/buildandrun.sh

if [ ! -d node_modules ]; then
	pnpm install
fi

if tsc; then
	node test/tests.js
else
	exit 1;
fi
