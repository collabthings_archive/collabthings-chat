var FlumeReduce = require( 'flumeview-reduce' );
var ref = require( 'ssb-ref' );

exports.name = 'collabthings-chat';
exports.version = '0.0.1';
exports.manifest = {
    stream: 'source',
    get: 'async'
}

exports.init = function( ssb: any, config: any ) {
    console.log( "CHAT plugin init " + ssb );
    return ssb._flumeUse( 'collabthings-chat', FlumeReduce( 1, chatsreduce, chatsmap ) );
}

function chatsreduce( result: any, item: any ) {
    console.log( "CHAT reduce " + JSON.stringify( result ) );

    if ( !result ) result = {}

    if ( item ) {
        console.log( "CHAT reduce item " + JSON.stringify( item ) );
        for ( var channel in item ) {
            var chat: any = item[channel];
            var valuesForSource = result[channel] = result[channel] || {}

            if ( typeof ( result[channel] ) == 'undefined' ) {
                console.log( "CHAT reduce unknown channel " + channel );
                result[channel] = {};
            }

            for ( var timestamp in chat ) {
                console.log( "CHAT reduce timestamp in chat " + timestamp );
                if ( typeof ( result[channel][timestamp] ) == 'undefined' ) {
                    result[channel][timestamp] = {};
                }

                for ( var source in chat[timestamp] ) {
                    result[channel][timestamp][source] = chat[timestamp][source];
                }
            }
        }
    }

    console.log("CHAT reduce current result " + JSON.stringify(result));

    return result
}

function chatsmap( msg: any ) {
    //console.log( "CHAT map " + JSON.stringify( msg ) );

    //&& ref.isLink( msg.value.content.contact )
    if ( msg.value.content && msg.value.content.type === 'collabthings-chat' ) {
        console.log( "CHAT map " + JSON.stringify( msg ) );

        var source = msg.value.author;
        var content = msg.value.content;
        // TODO really?
        var chats: { [key: string]: { [key: string]: { [key: string]: string } } } = {};

        var channel: string = content.channel;
        var message: string = content.message;
        var timestamp: string = msg.value.timestamp;

        console.log( "CHAT map chat value from " + source + " channel:" + channel + " message:" + message );

        chats[channel] = {};
        chats[channel][timestamp] = {};
        chats[channel][timestamp][source] = content;

        return chats;
    }
}

