#!/bin/bash

mkdir -p client/lib/css
mkdir -p client/lib/js

cp node_modules/jquery/dist/jquery.js client/lib/js/
cp node_modules/underscore/underscore.js client/lib/js/underscore.js
cp node_modules/vue-router/dist/vue-router.js client/lib/js/vue-router.js

