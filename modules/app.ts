import { CTApi, CTApp, CTSsb, CTAppInfo } from 'collabthings-api';
import express from 'express';
import path from 'path';
import * as bodyParser from 'body-parser';

export default class CTChatApp {
    app: CTApp;

    constructor( app: CTApp ) {
        console.log( "Setting up CTChat" );
        this.app = app;
    }

    getApi(): CTApi {
        return this.app.getApi();
    }

    stop() {
    }

    async addMessage( channel: string, message: string ) {
        console.log( "adding message " + channel + " - " + message );

        return this.app.getSsb().publish( {
            type: "collabthings-chat",
            channel: channel,
            message: "" + message
        } );
    }

    register( ctapi: CTApi ) {
        var self = this;

        var urlencodedParser = bodyParser.urlencoded( {
            extended: false
        } );

        console.log( "Registering ctchat" );
        var info: CTAppInfo = new CTAppInfo();
        info.name = "chat";
        info.type = "amazin'";
        info.launch = "chat/client/"
        info.settings = {
            that: "is",
            cnn: "OK?"
        };

        info.static = ( exp: express.Application ) => {
            var staticdir: string = path.join( __dirname, '../client' );
            console.log( "CTChat init static " + staticdir );
            exp.use( "/chat/client", express.static( staticdir, {
                setHeaders: function( res, path ) {
                    console.log( "ctchat request " + path );
                    if ( path.indexOf( "download" ) !== -1 ) {
                        res.attachment( path )
                    }
                },
            } ) );
        };

        info.api = ( exp: express.Application ) => {
            exp.get( "/chat/channels", function( req, res ) {
                res.send( "NOT IMPLEMENTED" );
            } );

            exp.get( "/chat/all", function( req, res ) {
                self.app.ssb.getSbot().collabthingsChat.get(( err: string, state: any ) => {
                    res.send( JSON.stringify( state ) );
                } );

            } );

            exp.post( "/chat/post", urlencodedParser, function( req, res ) {
                console.log( "POST " + JSON.stringify( req.body ) );
                var channel: string = req.body.channel;
                var message: string = req.body.message;

                self.addMessage( channel, message ).then(() => {
                    res.send( "OK" );
                } ).catch(( err: string ) => {
                    console.log( "Posting failed " + JSON.stringify( err ) );
                    res.send( "FAIL " + JSON.stringify( err ) );
                } );
            } );
        };

        ctapi.addApp( info );
        console.log( "Registered ctchat" );
    }
}
