import querystring from 'querystring';
import http from 'http';
import fs from 'fs';

import path from 'path';

export default class CTChatClient {
    port: Number;
    host: string = "localhost";

    constructor( port: Number ) {
        this.port = port;
    }

    sendMessage( channel: String, message: String ) {
        var post_data = querystring.stringify( {
            'channel': channel,
            'message': message
        } );

        var post_options = {
            host: this.host,
            port: "" + this.port,
            path: '/chat/post',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength( post_data )
            }
        };

        var post_req = http.request( post_options, function( res ) {
            res.setEncoding( 'utf8' );
            res.on( 'data', function( chunk ) {
                console.log( 'Response: ' + chunk );
            } );
        } );

        // post the data
        post_req.write( post_data );
        post_req.end();
    }
}
